[docker-for-mac](https://www.docker.com/docker-mac) 이나 [docker-for-windows](https://www.docker.com/docker-windows) 를 사용해서 실행하세요


아래의 커맨드로 실행하십시오  
`
docker run -d -p 8080:8080 cinos81/visualization
`

로컬에서의 index.html의 위치는 아래와 같습니다.  
`
http://localhost:8080/jsonspec/index.html
`